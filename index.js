db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnSale" }]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      totalFruits: { $sum: "$stock" },
    },
  },
  { $count: "enoughStock" },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } },
]);
